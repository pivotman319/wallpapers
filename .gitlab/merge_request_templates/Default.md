<!--
WAIT!
Before you submit this MR:
[ ] Does it avoid using banned formats?
[ ] You either created it or have permission to submit it?
[ ] Is it SFW? 
-->

[ ] I agree to license these files under CC BY-NC-SA 4.0 and that I have full permission to do so.
